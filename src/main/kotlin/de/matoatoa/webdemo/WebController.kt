package de.matoatoa.webdemo

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

/**
 * @author Jan Bormann
 */

@RestController
class WebController{
    @GetMapping("") fun root () = "Hallo Welt"

    @GetMapping("/hallo/{name}") fun hallo (@PathVariable name : String) = "Hallo $name"
}