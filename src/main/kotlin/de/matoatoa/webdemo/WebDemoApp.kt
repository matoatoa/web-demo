package de.matoatoa.webdemo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 * @author Jan Bormann
 */

@SpringBootApplication
class WebDemoApp

fun main(args: Array<String>) {
    SpringApplication.run(WebDemoApp::class.java, *args)
}
