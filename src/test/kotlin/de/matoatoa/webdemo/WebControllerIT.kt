package de.matoatoa.webdemo

import org.hamcrest.CoreMatchers.containsString
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content

/**
 * @author Jan Bormann
 */

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class WebControllerIT {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Test
    fun isUnauthorized (){
        mockMvc.perform(get("/")).andExpect(status().isUnauthorized)
    }

    @Test
    @WithMockUser("user")
    fun getRoot (){
        mockMvc.perform(get("/")).andExpect(status().isOk).andExpect(content().string(containsString("Hallo Welt")))
    }

    @Test
    @WithMockUser("user")
    fun getHallo (){
        mockMvc.perform(get("/hallo/Jan")).andExpect(status().isOk).andExpect(content().string(containsString("Hallo Jan")))
    }

    @Test
    @WithMockUser("user")
    fun getUnknown (){
        mockMvc.perform(get("/unknown")).andExpect(status().isNotFound)
    }
}
