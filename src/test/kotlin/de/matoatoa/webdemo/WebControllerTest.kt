package de.matoatoa.webdemo

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * @author Jan Bormann
 */
class WebControllerTest {

    private val controller = WebController()

    @Test
    fun hallo() {
        assertEquals("Begrüßungstext stimmt nicht", "Hallo Jan", controller.hallo("Jan"))
    }

    @Test
    fun root() {
        assertEquals("Root-Text", "Hallo Welt", controller.root())
    }
}