package de.matoatoa.webdemo

import junit.framework.TestCase.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

/**
 * @author Jan Bormann
 */

@SpringBootTest
@RunWith(SpringRunner::class)
class WebDemoAppIT {

    @Autowired
    lateinit var webController : WebController

    @Test
    fun loadContext(){
        assertNotNull("WebController ist null", webController)
    }
}